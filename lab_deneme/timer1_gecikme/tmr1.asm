INCLUDE <P16F877A.INC>
sayac equ 0x20
sayac1 equ 0x21
goto anaprogram
org 0x04
kesme
	BANKSEL PIR1
	BCF PIR1,TMR1IF
	BANKSEL TMR1L
	movlw h'3C'
	movwf TMR1H
	movlw h'B0'
	movwf TMR1L
	incf sayac1
	movf sayac1,w
	sublw d'20'
	btfss STATUS,Z
	retfie
	clrf sayac1
	comf sayac,f
	retfie
anaprogram
		BANKSEL T1CON
		bsf T1CON,TMR1ON
		bcf T1CON,4
		bcf T1CON,5
	    bcf T1CON,TMR1CS
		bcf T1CON,T1SYNC
		BANKSEL TMR1L
		movlw h'3C'
		movwf TMR1H
		movlw h'B0'
		movwf TMR1L
		BANKSEL INTCON
		bsf INTCON,PEIE
		BSF INTCON,GIE
		BANKSEL PIE1
		BSF PIE1,TMR1IE
		BANKSEL TRISB
		CLRF TRISB
		BANKSEL PORTB
		clrf sayac
		clrf sayac1

a
movf sayac,w
MOVWF PORTB
goto a
END