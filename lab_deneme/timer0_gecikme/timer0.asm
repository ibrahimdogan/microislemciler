INCLUDE <P16F877A.INC>
goto anaprogram
sayac equ 0x20
sayac1 equ 0x23
sayi equ 0x21
yedek equ 0x22
org 0x04
kesme
BCF INTCON,2
incf sayac
MOVLW D'125'
SUBWF sayac,w
btfss STATUS,C
retfie
clrf sayac
;rlf sayi,f
;btfss sayi,7
;retfie
;movlw d'1'
movwf sayi
;;movf sayi,w
;;movwf yedek
;addwf sayi,f
RETFIE

anaprogram
BANKSEL INTCON
BSF INTCON,GIE
;BSF INTCON,PEIE
BSF INTCON,5
BANKSEL OPTION_REG
MOVLW B'00000100'
MOVWF OPTION_REG
BANKSEL TMR0
MOVLW D'5'
MOVWF TMR0
BANKSEL TRISB
CLRF TRISB
BANKSEL PORTB
movlw d'1'
movwf sayi
movf yedek

a
rlf sayi,f
MOVF sayi,w
movwf PORTB

call gecikme
goto a


gecikme
movlw d'255'
movwf sayac1
gecikme1
	movlw d'255'
	movwf sayac
	decfsz sayac,f
	goto $-1
	decfsz sayac1,f
	goto gecikme1
	return

END