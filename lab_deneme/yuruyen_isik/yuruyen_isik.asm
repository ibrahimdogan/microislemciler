INCLUDE <P16F877A.INC>
deger equ 0x20
gecik equ 0x21
gecik1 equ 0x22
org 0x04
anaprogram
BANKSEL TRISB
CLRF TRISB
BANKSEL PORTB
movlw d'0'
movwf deger
a
	movf deger,w
	movwf PORTB
	rrf deger,f
	call gecikme
	call gecikme
goto a

gecikme
		movlw d'255'
		movwf gecik
		gecikme1
		movlw d'255'
		movwf gecik1
		decfsz gecik1,f
		goto $-1
		decfsz gecik,f
		goto gecikme1
		return
END