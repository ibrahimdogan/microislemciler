INCLUDE <P16F877A.INC>
sayac equ 0x20
gecik equ 0x21
org 0x04
menu
BANKSEL TRISB
CLRF TRISB
clrf TRISC
BANKSEL PORTB
CLRF PORTB
clrf sayac
a
movf sayac,w
call table2
movwf PORTC
movf sayac,w
call table1
movwf PORTB
call gecikme
incf sayac
movlw d'5'
subwf sayac,w
btfsc STATUS,Z
clrf sayac
goto a

table2
addwf PCL,F
retlw d'1'
retlw d'2'
retlw d'4'
retlw d'8'

table1
addwf PCL,F
retlw d'0'
retlw b'00110110'
retlw b'00110110'
retlw b'01000001'
return

gecikme
movlw d'200'
movwf gecik
decfsz gecik,f
goto $-1
return

END