INCLUDE <P16F877A.INC>
sayac equ 0x20

org 0x00
goto anaprogram
org 0x04
kesme


BANKSEL TXREG
movlw 'S'
MOVWF TXREG
movlw 'O'
MOVWF TXREG
movlw 'S'
MOVWF TXREG
BANKSEL PIR1
BCF PIR1,TXIF
retfie

anaprogram
;BANKSEL TRISB
;CLRF TRISB
;CLRF TRISD
;BANKSEL PORTB
;call komut_gonder
;call karakter_gonder
call usart_ayarla


gt
BANKSEL PIR1
BTFSS PIR1,TXIF
GOTO gt
a
BANKSEL TXREG
MOVLW 'K'
movwf TXREG
CALL gecikme
MOVLW 'E'
movwf TXREG
CALL gecikme
MOVLW 'K'
movwf TXREG
call gecikme
BCF PIR1,TXIF
goto a

komut_gonder
		movlw 0x02
		call komut_yaz
		movlw 0x38
		call komut_yaz
		movlw 0x0C
		call komut_yaz
		return
karakter_gonder
		movlw 'i'
		call karakter_yaz
		movlw 'b'
		call karakter_yaz
		movlw 'r'
		call karakter_yaz
		movlw 'h'
		call karakter_yaz
		movlw 'i'
		call karakter_yaz
		movlw 'm'
		call karakter_yaz
komut_yaz
		movwf PORTD
		bcf PORTB,0
		BSF PORTB,1
		call gecikme
		bcf PORTB,1
		RETURN

karakter_yaz
		movwf PORTD
		bsf PORTB,0
		BSF PORTB,1
		call gecikme
		bcf PORTB,1
		RETURN
usart_ayarla
	banksel INTCON
;	BSF INTCON,GIE
	BSF INTCON,PEIE
	BANKSEL PIE1
	BSF PIE1,TXIE
	BANKSEL RCSTA 
	MOVLW B'10010000'
	MOVWF RCSTA
	BANKSEL TXSTA
	MOVLW B'00100100'
	MOVWF TXSTA
	BANKSEL SPBRG
	MOVLW D'51'
	movwf SPBRG
	return


gecikme
		movlw d'255'
		movwf sayac
		decfsz sayac,f
		goto $-1
	movlw d'255'
		movwf sayac
		decfsz sayac,f
		goto $-1
		movlw d'255'
		movwf sayac
		decfsz sayac,f
		goto $-1
		return

END