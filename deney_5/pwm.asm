__config h'3f31'
INCLUDE <P16F877A.INC>
anaprogram
	BANKSEL PR2
	MOVLW D'249'
	MOVWF PR2
	BANKSEL CCP1CON
	MOVLW D'12'
	MOVWF CCP1CON
	MOVWF CCP2CON
	MOVLW D'125'
	MOVWF CCPR2L
	MOVWF CCPR1L
	BANKSEL TRISC
	clrf TRISC
	movlw d'255'
	movwf TRISA
	movlw 0x06
	movwf ADCON1
	BSF INTCON,GIE
	BSF INTCON,PEIE
	BANKSEL T2CON
	MOVLW D'5'
	MOVWF T2CON

	don
	BTFSC PORTA,0
	GOTO motor_artir
	btfsc PORTA,1
	GOTO motor_azalt
	BTFSC PORTA,2
	GOTO led_artir
	btfsc PORTA,3
	GOTO led_azalt
	GOTO don
	
motor_artir
	MOVF CCPR2L,W
	SUBLW D'245'
	btfss STATUS,C
	goto don
	MOVLW D'5'
	ADDWF CCPR2L,f
	goto sonsuz
motor_azalt
	movlw d'5'
	SUBWF CCPR2L,W
	BTFSS STATUS,C
	goto don
 	MOVlW d'5'
	subwf CCPR2L,F
	goto sonsuz
led_artir
	MOVlW d'5'
	addwf CCPR1L,F
	goto sonsuz
led_azalt
 	MOVlW d'5'
	subwf CCPR1L,F
	goto sonsuz
sonsuz
	btfsc PORTA,0
	goto sonsuz
	btfsc PORTA,1
	goto sonsuz
	btfsc PORTA,2
	goto sonsuz
	btfsc PORTA,3
	goto sonsuz
	goto don
	end
