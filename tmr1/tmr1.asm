INCLUDE <P16F877A.INC>
		sayac equ 0x20
		secim equ 0x21
		hizbelirle equ 0x22
		CLRF PCLATH
		goto anaprogram
org 0x04
kesme
		btfss PIR1,TMR1IF
		retfie
		bcf PIR1,0
		banksel TMR1H
		MOVLW d'55'
		movwf TMR1H
		MOVLW d'5'
		movwf TMR1L
		incf sayac
		movf sayac,w
		subwf hizbelirle,w
		btfss STATUS,Z
		retfie
		goto ayarla
		retfie
anaprogram
		BANKSEL TRISB
		movlw 0x06
		movwf ADCON1
		movlw d'255'
		movwf TRISA
		CLRF TRISB
		BANKSEL T1CON
		MOVLW d'55'
		movwf TMR1H
		MOVLW d'5'
		movwf TMR1L
		movlw 0x20
		movwf T1CON
		bcf T1CON,TMR1CS
		bcf T1CON,T1SYNC
		bsf T1CON,0
		bcf T1CON,4
		bcf T1CON,5
		banksel PIE1
		bsf PIE1,0
		banksel INTCON
		bsf INTCON,6
		bsf INTCON,7
		
		degerata
		clrf secim
		clrf sayac
		movlw d'20'
		movwf hizbelirle
yak
		btfsc PORTA,0
		goto hizartir
		btfsc PORTA,1
		goto hizazalt
		movf secim,w
		movwf PORTB
		goto yak
ayarla
		incf secim,f
		clrf sayac
		retfie
hizartir
		movf hizbelirle,w
		sublw d'10'
		btfsc STATUS,Z
		goto yak
		bcf STATUS,Z
		decf hizbelirle
		decf hizbelirle
sonsuz1
		btfss PORTA,0
		goto yak
		goto sonsuz1


hizazalt
	    movf hizbelirle,w
		sublw d'30'
		btfsc STATUS,Z
		goto yak
		bcf STATUS,Z
		incf hizbelirle
		incf hizbelirle
sonsuz2
		btfss PORTA,1
		goto yak
		goto sonsuz2
END