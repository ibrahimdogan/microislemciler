INCLUDE <P16F877A.INC>
sayac equ 0x20
goto anaprogram
org 0x04
kesme
BTFSS INTCON,TMR0IF
retfie
comf sayac,f
movlw d'230'
movwf TMR0
bcf INTCON,TMR0IF
retfie


org 0x10
anaprogram
BSF STATUS,RP0
BSF STATUS,RP1
BSF INTCON,GIE
BSF INTCON,TMR0IE
MOVLW H'00'
MOVWF OPTION_REG
bcf STATUS,RP1
CLRF TRISB
BCF STATUS,RP0
movlw d'230'
movwf TMR0
movlw d'255'
movwf sayac
bcf STATUS,RP0

yak
movf sayac,w
movwf PORTB
goto yak

END