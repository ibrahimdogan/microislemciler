INCLUDE <P16F877A.INC>
veri equ 0x20
sayac equ 0x21
goto anaprogram
org 0x04

anaprogram
	call ilk_islem
	bcf INTCON,GIE
	
anaj1
	call karakter_al
	movwf veri
	incf veri
anaji
	call karakter_gonder
	movlw d'48'
	movwf veri
	goto anaj1

karakter_gonder
	banksel PIR1
	BTFSS PIR1,TXIF
	GOTO $-1
	bcf PIR1,TXIF
	MOVF veri,w
	BANKSEL TXREG
	MOVWF TXREG
	RETURN
karakter_al
	BANKSEL PIR1
	BTFSS PIR1,RCIF
	GOTO $-1
	BCF PIR1,RCIF
	MOVF RCREG,W
	RETURN
ilk_islem

	banksel TRISB
	bcf TRISC,6
	BSF TRISC,7
	BANKSEL PORTB


	banksel SPBRG
	MOVLW D'25'
	movwf SPBRG
	BANKSEL TXSTA
	MOVLW d'26'
	MOVWF TXSTA
	MOVWF TXSTA
	BANKSEL RCSTA
	movlw d'90'
	movwf RCSTA
	BANKSEL INTCON
	BSF INTCON,GIE
	BSF INTCON,PEIE
	BANKSEL PIE1
	BSF PIE1,TXIE
	BSF PIE1,RCIE
	return

END