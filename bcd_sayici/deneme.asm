INCLUDE	<P16F877A.INC>
org 0x021

anaprogram
bsf STATUS,RP0
movlw 0x06
movwf ADCON1
movlw b'11111111'
movwf TRISA
movlw d'0'
movwf TRISB
bcf STATUS,RP0
clrf PORTB
movlw b'11111111'
movwf PORTA


don
btfss PORTA,1
goto azalt
goto artir

azalt
btfss PORTA,2
GOTO don
decf PORTB
goto sonsuz2

artir
incf PORTB
goto sonsuz

sonsuz
BTFSS PORTA,1
GOTO don
goto sonsuz

sonsuz2
btfss PORTA,2
goto don
goto sonsuz2
END