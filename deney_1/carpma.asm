__config H'3f31'
INCLUDE <P16F877A.INC>
sayi1 equ 0x21
sayi2 equ 0x22
sayi_l equ 0x23
sayi_h equ 0x24

org 0x04
anaprogram
	bsf STATUS,RP0
	movlw 0x06
	movwf ADCON1
	movlw d'255'
	movwf TRISA
	movlw d'0'
	movwf TRISB
	movwf TRISC
	bcf STATUS,RP0
	movlw d'3'
	movwf sayi1
	movlw d'173'
	movwf sayi2
	clrf PORTB
	bsf PORTC,0
	clrf sayi_h
	clrf sayi_l
	bcf STATUS,C
	clrw

dongu
	btfsc STATUS,C
	goto ayarla
	addwf sayi1,w
	decfsz sayi2,f
	goto dongu
	goto yak
	
ayarla
	incf sayi_h
	bcf STATUS,C
	goto dongu
yak
	btfsc STATUS,C
	incf sayi_h
	movwf sayi_l
	movwf PORTB
	bcf STATUS,C
	clrf sayi1
	goto kontrol
kontrol
	btfss PORTA,0
	goto kontrol
	goto secim_l
secim_l
	btfss sayi1,0
	goto secim_h
	bsf PORTC,0
	movf sayi_l,w
	movwf PORTB
	comf sayi1,f
	goto sonsuz
secim_h
	bcf PORTC,0
	movf sayi_h,w
	movwf PORTB
	comf sayi1,f
	goto sonsuz
sonsuz
	btfss PORTA,0
	goto kontrol
	goto sonsuz	
END