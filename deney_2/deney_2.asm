__config h'3f31'
include <P16F877A.INC>
list p=16F877A
sayac equ 0x20
deger equ 0x21
isikhizi equ 0x22
goto anaprogram
org 0x04
kesme
		banksel PIR1
		btfss PIR1,TMR1IF
		retfie
		MOVLW h'3C'
		MOVWF TMR1H
		MOVLW h'B0'
		MOVWF TMR1L
		bcf PIR1,TMR1IF
		incf sayac
		movf sayac,w
		subwf isikhizi,w
		btfss STATUS,Z
		retfie
		incf deger
		clrf sayac
		retfie

anaprogram
		BANKSEL T1CON
		bsf T1CON,TMR1ON
		bcf T1CON,4
		bcf T1CON,5
	    bcf T1CON,TMR1CS
		bcf T1CON,T1SYNC
		BANKSEL INTCON
		bsf INTCON,6
		bsf INTCON,7
		banksel PIE1
		bsf PIE1,0
		BANKSEL TRISB
		clrf TRISB
		movlw 0x06
		movwf ADCON1
		movlw d'255'
		movwf TRISA
		BANKSEL PORTB
		CLRF PORTB
		clrf deger
		clrf sayac
		movlw d'20'
		movwf isikhizi
		BANKSEL TMR1L
		MOVLW h'3C'
		MOVWF TMR1H
		MOVLW h'B0'
		MOVWF TMR1L
yak
		movf deger,W
		movwf PORTB
		btfsc PORTA,1
		goto hizlandir
		BTFSC PORTA,0
		goto yavaslat
		goto yak

yavaslat
		movf isikhizi,w
		sublw d'30'
		btfsc STATUS,Z
		goto yak
		incf isikhizi
		incf isikhizi
		goto sonsuz

hizlandir
		btfsc isikhizi,4
    	goto a
		btfss isikhizi,3
		goto a
		btfsc isikhizi,2
		goto a
		btfss isikhizi,1
		goto a
		goto yak
a
		decf isikhizi
		decf isikhizi
		goto sonsuz
sonsuz
		BTFSC PORTA,0
		GOTO sonsuz
		BTFSC PORTA,1
		goto sonsuz
		goto yak
END