INCLUDE <P16F877A.INC>
gecik1 equ 0x20
gecik2 equ 0x21
sayi equ 0x22
ORG 0X00
anaprogram
BANKSEL TRISB
CLRF TRISD
CLRF TRISB
BANKSEL PORTB
CLRF PORTB
CLRF PORTD
call komut_gonder
call karakter_gonder


a

goto a

komut_gonder
movlw 0x02
call komut_yaz
movlw 0x38
call komut_yaz
movlw 0x0c
call komut_yaz
return


karakter_gonder
movf sayi,w
call table1
call karakter_yaz
incf sayi
movlw d'7'
subwf sayi,w
btfss STATUS,Z
goto karakter_gonder
movlw 0XC0
call komut_yaz
clrf sayi



karakter_gonder2
movf sayi,w
call table2
call karakter_yaz
incf sayi
movlw d'5'
subwf sayi,w
btfss STATUS,Z
goto karakter_gonder2
return



komut_yaz
	movwf PORTD
	BCF PORTB,0
	BSF PORTB,1
	call gecikme
	bcf PORTB,1
	RETURN	

karakter_yaz
	movwf PORTD
	BSF PORTB,0
	BSF PORTB,1
	call gecikme
	bcf PORTB,1
	RETURN

gecikme
movlw d'255'
movwf gecik1
decfsz gecik1,f
goto $-1
return


table1
addwf PCL,F
retlw 'i'
retlw 'b'
retlw 'r'
retlw 'a'
retlw 'h'
retlw 'i'
retlw 'm'

table2
addwf PCL,F
retlw 'd'
retlw 'o'
retlw 'g'
retlw 'a'
retlw 'n'

	
END