__config h'3f31'
INCLUDE <P16F877A.INC>
sayi equ 0x20
sayi1 equ 0x21
goto anaprogram
org 0x04
kesme
	BANKSEL PIR1
	bcf PIR1,RCIF
	banksel PIR1
	bcf PIR1,TXIF
	movf RCREG,W
	CALL karakter_yaz
	retfie
anaprogram
	call port_ayarla
	call komut_gonder
	;call karakter_gonder
	call usart_ayarla
menu
BANKSEL TXREG
movlw 'S'
movwf TXREG
call gecikme
movlw 'O'
MOVWF TXREG
call gecikme
movlw 'S'
MOVWF TXREG	
call gecikme
goto menu
a

goto a

port_ayarla
	banksel TRISB
	CLRF TRISD
	CLRF TRISB
	movlw 0xff
	movwf TRISC
	BANKSEL PORTB
	bsf PORTB,0
	BCF PORTB,1
	CLRF PORTC
	CLRF PORTD
	return
komut_gonder
	movlw 0x02
	call komut_yaz
	movlw 0x38
	call komut_yaz
	movlw 0x0C
	call komut_yaz	
karakter_gonder
	movlw 'S'
	CALL karakter_yaz
	movlw 'O'
	call karakter_yaz
	movlw 'O'
	call karakter_yaz
	return
komut_yaz
	movwf PORTD
	bcf PORTB,2
	bsf PORTB,3
	call gecikme
	bcf PORTB,3
	return

karakter_yaz
	movwf PORTD
	bsf PORTB,2
	bsf PORTB,3
	call gecikme
	bcf PORTB,3
	return
usart_ayarla
	banksel SPBRG
	MOVLW D'25'
	movwf SPBRG
	BANKSEL TXSTA
	MOVLW B'00000100'
	MOVWF TXSTA
	MOVWF TXSTA
	BANKSEL RCSTA
	movlw b'10010000'
	movwf RCSTA
	BANKSEL INTCON
	BSF INTCON,GIE
	BSF INTCON,PEIE
	BANKSEL PIE1
	BSF PIE1,RCIE
	return
gecikme
	movlw d'255'
	movwf sayi
gecikme2
	movlw d'40'
	movwf sayi1
	decfsz sayi1,f
	goto $-1
	decfsz sayi,f
	goto gecikme2
	return
END