INCLUDE <P16F877A.INC>
sayi1 equ 0x21
sayi2 equ 0x22
sayi3 equ 0x23
org 0x04

anaprogram
bsf STATUS,RP0
clrf TRISB
clrf TRISC
bcf STATUS,RP0
clrf PORTB
clrf PORTC
movlw d'8'
movwf sayi1
movlw d'128'
movwf sayi2
clrw
bcf STATUS,C
clrf sayi3

topla
BTFSC STATUS,C
goto bosalt
ADDWF sayi2,w
decfsz sayi1,1
goto topla
goto sonuc

bosalt
incf sayi3
bcf STATUS,C
goto topla

sonuc
BTFSC STATUS,C
incf sayi3
movwf PORTB
movf sayi3,0
movwf PORTC




END
clrw
