__config h'3f31'
INCLUDE <P16F877A.INC>
sayac equ 0x20
deger1 equ 0x21
deger2 equ 0x25
a equ 0x26

goto anaprogram
org 0x04
kesme
		BTFSS INTCON,TMR0IF
		retfie
		movlw d'5'
		movwf TMR0
		BANKSEL INTCON
		bcf INTCON,TMR0IF
		incf sayac
		movlw d'125'
		subwf sayac,w
		btfss STATUS,Z
		retfie
		call ayarla
		retfie
org 0x50
anaprogram
		BSF STATUS,RP0
		BSF STATUS,RP1;bank 3 e gec
		BSF INTCON,GIE
		BSF INTCON,TMR0IE  ;tmr0 aktifet
		MOVLW b'00000100'
		MOVWF OPTION_REG  ;presckler 32 ayarla
		BANKSEL TRISA  
		CLRF TRISB
		clrf TRISA  
		BCF STATUS,RP0
		movlw d'5'
		movwf TMR0
yak
		call sonsuz
		movlw d'2'
		movwf PORTA
		movf deger1,w
		call looktable
		movwf PORTB
		call sonsuz
		movlw d'1'
		movwf PORTA
		movf deger2,w
		call looktable
		movwf PORTB
		goto yak
sonsuz
		movlw d'250'
		movwf a
		decfsz a
		goto $-1
		return
ayarla
		clrf sayac
		incf deger1
		incf deger1
		movf deger1,w
		sublw d'10'
		btfss STATUS,Z
		return
		clrf deger1
		incf deger2
		movf deger2,w
		sublw d'4'
		btfss STATUS,Z
		return
		clrf deger2
		return		
looktable
		addwf PCL,f
		retlw b'0111111'
		retlw b'0000110'
		retlw b'1011011'
		retlw b'1001111'
		retlw b'1100110';4
		retlw b'1101101';5
		retlw b'1111101';6
		retlw b'0000111'
		retlw b'1111111'
		retlw b'1101111'
END
