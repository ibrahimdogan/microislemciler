__config h'3f31'
INCLUDE <P16F877A.INC>
sayac equ 0x20
sayac2 equ 0x21
gecik equ 0x22
yedek equ 0x23
birler equ 0x24
onlar equ 0x25
yuzler equ 0x26
basamak_yedek equ 0x27
alt_birler equ 0x28
alt_onlar equ 0x29
alt_yuzler equ 0x30
buton_sayma equ 0x31
goto anaprogram
org 0x04
kesme
	btfss INTCON,2
	retfie
	bcf INTCON,2
	MOVLW D'6'
	MOVWF TMR0
	incf sayac2
	movlw d'125'
	subwf sayac2,w
	btfss STATUS,Z
	retfie
	incf sayac,f
	movlw d'200'
	subwf sayac,w
	btfsc STATUS,Z
	clrf sayac
	call degistir
	clrf sayac2
	retfie
anaprogram
	call tmr2_ayar
	call komut_gonder
	call karakter_gonder
main
	btfsc PORTA,0
	goto alt_degistir
    btfss PORTA,1
	goto main
	decf buton_sayma
	decf buton_sayma
goto alt_degistir


degistir
		clrf yuzler
		clrf onlar
		clrf birler
		call komut_gonder
		movf sayac,w
		movwf birler
		call yuzler_basamak
		call karakter_gonder
		return
alt_degistir
		clrf alt_yuzler
		clrf alt_onlar
		clrf alt_birler
		call komut_gonder
		incf buton_sayma
		movf buton_sayma,w
		movwf alt_birler
		call alt_yuzler_basamak
		call karakter_gonder
		goto sonsuz
komut_gonder
		movlw 0x02
		call komut_yaz
		movlw 0x28
		call komut_yaz
		movlw 0x0C
		call komut_yaz
		movlw 0x80
		call komut_yaz
		return
karakter_gonder
		movf alt_yuzler,w
		addlw d'48'
		call karakter_yaz
		movf alt_onlar,w
		addlw d'48'
		call karakter_yaz
		movf alt_birler,w
		addlw d'48'
		call karakter_yaz
		movlw 0xC0
		call komut_yaz
		movf yuzler,w
		addlw d'48'
		call karakter_yaz
		movf onlar,w
		addlw d'48'
		call karakter_yaz
		movf birler,w
		addlw d'48'
		call karakter_yaz
	    return

komut_yaz
		movwf yedek
		swapf yedek,w
		
		andlw 0x0f
		movwf PORTB
		bcf PORTB,4
		BSF PORTB,5
		CALL gecikme
		bcf PORTB,5
		
		MOVF yedek,w
		andlw 0x0f
		movwf PORTB
		bcf PORTB,4
		BSF PORTB,5
		CALL gecikme
		bcf PORTB,5
		return

karakter_yaz
		movwf yedek
		swapf yedek,w
		
		andlw 0x0f
		movwf PORTB
		bsf PORTB,4
		BSF PORTB,5
		CALL gecikme
		bcf PORTB,5
		
		MOVF yedek,w
		andlw 0x0f
		movwf PORTB
		bsf PORTB,4
		BSF PORTB,5
		CALL gecikme
		bcf PORTB,5
		return

tmr2_ayar
		banksel INTCON
		BSF INTCON,GIE
		BSF INTCON,TMR0IE
		BANKSEL OPTION_REG
		MOVLW D'1'
		MOVWF OPTION_REG
		BANKSEL TMR0
		MOVLW D'6'
		MOVWF TMR0
		BANKSEL TRISB
		CLRF TRISB
		movlw d'255'
		movwf TRISA
		MOVLW 0X06
		MOVWF ADCON1
		banksel PORTB
		clrf sayac
		clrf alt_birler
		clrf alt_onlar
		movlw d'1'
		movwf alt_yuzler
		movlw d'100'
		movwf buton_sayma
		
		return
yuzler_basamak
		movlw d'100'
		subwf birler,f
        incf yuzler
		btfsc STATUS,C
		goto yuzler_basamak
		movlw d'100'
		addwf birler,f
		decf yuzler
onlar_basamak
		movlw d'10'
		subwf birler,f
		incf onlar
		btfsc STATUS,C
		goto onlar_basamak
		movlw d'10'
		addwf birler
		decf onlar
		return
alt_yuzler_basamak
		movlw d'100'
		subwf alt_birler,f
        incf alt_yuzler
		btfsc STATUS,C
		goto alt_yuzler_basamak
		movlw d'100'
		addwf alt_birler,f
		decf alt_yuzler
alt_onlar_basamak
		movlw d'10'
		subwf alt_birler,f
		incf alt_onlar
		btfsc STATUS,C
		goto alt_onlar_basamak
		movlw d'10'
		addwf alt_birler
		decf alt_onlar
		return
gecikme
		movlw d'255'
		movwf gecik
		decfsz gecik,f
		goto $-1
		return		
sonsuz
		btfsc PORTA,0
		goto sonsuz
		btfsc PORTA,1
		goto sonsuz
		goto main
END